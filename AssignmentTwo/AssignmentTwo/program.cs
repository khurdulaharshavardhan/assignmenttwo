﻿using System;

namespace AssignmentTwo
{
	public class Program
	{

		public void DisplayMenu()
        {
			Console.WriteLine("------------------------");
			Console.WriteLine("Please choose the operation to be performed: ");
			Console.WriteLine("1. Add two Integers.");
			Console.WriteLine("2. Add three Integers.");
			Console.WriteLine("3. Add two Floating point numbers.");
			Console.WriteLine("4. Find power of two integers: ");
			Console.WriteLine("5. Exit");
			Console.WriteLine("Please enter your choice: ");
		}
		
		public void DisplayResult(double value)
        {
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("The result of the operation is: {0}", value);
			Console.ResetColor();
		}
		static void Main(string[] args)
		{
			Program driver = new Program();
			Calculator calculator = new Calculator();
			AdvancedCalculator aCalculator = new AdvancedCalculator();

            //menu has to run till user decides to exit..
            while (true)
            {
				var choice=0;

				//display the menu and obtain an response from the user:
				driver.DisplayMenu();
				try
				{
					 choice = Convert.ToInt32(Console.ReadLine().Trim());
				}
				catch (Exception) {
					Console.WriteLine("Invalid Input!");
					continue;
				}

				if (choice == 5)
				{
					break;
				}
				else {
                    switch (choice)
                    {
						case 1: //add two integers
							Console.WriteLine("Enter fisrt integer: ");
							var x = Convert.ToInt32(Console.ReadLine().Trim());
							Console.WriteLine("Enter second integer: ");
							var y = Convert.ToInt32(Console.ReadLine().Trim());

							calculator.Add(x, y);

							driver.DisplayResult(calculator.GetResult());
							break;

						case 2: //add three integers
							Console.WriteLine("Enter fisrt integer: ");
							 x = Convert.ToInt32(Console.ReadLine().Trim());
							Console.WriteLine("Enter second integer: ");
							 y = Convert.ToInt32(Console.ReadLine().Trim());
							Console.WriteLine("Enter third integer: ");
							var z = Convert.ToInt32(Console.ReadLine().Trim());

							calculator.Add(x, y, z);

							driver.DisplayResult(calculator.GetResult());
							break;

						case 3: //add two floating numbers
							Console.WriteLine("Enter fisrt floating number: ");
							var p = Convert.ToDouble(Console.ReadLine().Trim());
							Console.WriteLine("Enter second floating number: ");
							var q = Convert.ToDouble(Console.ReadLine().Trim());
							

							calculator.Add(p, q);

							driver.DisplayResult(calculator.GetResult());
							break;

						case 4: //find power of int a to int b.
							Console.WriteLine("Enter fisrt integer: ");
							x = Convert.ToInt32(Console.ReadLine().Trim());
							Console.WriteLine("Enter second integer: ");
							y = Convert.ToInt32(Console.ReadLine().Trim());

							aCalculator.Power(x, y);

							driver.DisplayResult(aCalculator.GetResult());
							break;


						default:
							Console.WriteLine("Invalid Input!");
							break;
					}
					
				}

				
			}
		}
	}
}
