﻿using System;

namespace AssignmentTwo
{
    public class Calculator
    {
        private double _result;
        

        public Calculator() {
            this._result = 0;
        }

        public void SetResult(double value) {
            this._result = value;
        }
        public double GetResult() {
            return this._result;
        }

        public void Add(int firstNumber, int secondNumber) {
            this._result = firstNumber + secondNumber;
        }

        public void Add(int firstNumber, int secondNumber,int thirdNumber)
        {
            this._result = firstNumber + secondNumber + thirdNumber;
        }

        public void Add(double firstNumber, double secondNumber)
        {
            this._result = firstNumber + secondNumber;
        }
    }
}
