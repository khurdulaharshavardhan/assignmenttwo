﻿using System;

namespace AssignmentTwo
{
	public class AdvancedCalculator : Calculator
	{
       
        public void Power(int basee, int exponent)
        {
            SetResult(Math.Pow(basee, exponent));
        }

        public new double GetResult()
        {
            return (base.GetResult() * Math.Pow(10, 6));
        }



    }
}

